﻿using System;
using System.Collections.Generic;
using HelloWorld;

namespace HelloWorldWeb.Models
{
    public class HelloWorldIndexModel
    {
        public List<HelloMessage> Messages { get; set; }

        public DateTime QueryTime { get; set; }
    }
}