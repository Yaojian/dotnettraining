﻿using System;
using System.Web;
using System.Web.Mvc;
using HelloWorld;
using HelloWorldWeb.Models;

namespace HelloWorldWeb.Controllers
{
    public class HelloWorldController : Controller
    {
        // GET: HelloWorld
        public ActionResult Index()
        {
            var model = CreateIndexViewModel();
            return View("Index", model);
        }

        public ActionResult Add(String message)
        {
            var helloMessageManager = new HelloMessageManager();
            helloMessageManager.AddMessage(message);
            var model = CreateIndexViewModel();
            return View("Index", model);
        }

        public ActionResult Delete(int id)
        {
            var helloMessageManager = new HelloMessageManager();
            helloMessageManager.DeleteMessage(id);
            var model = CreateIndexViewModel();
            return View("Index", model);
        }

        private static HelloWorldIndexModel CreateIndexViewModel()
        {
            var helloMessageManager = new HelloMessageManager();
            var model = new HelloWorldIndexModel();
            model.Messages = helloMessageManager.GetMessages();
            model.QueryTime = new DateTime(1999, 01, 02);
            return model;
        }
    }
}