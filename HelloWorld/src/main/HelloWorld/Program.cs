﻿using System;

namespace HelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            var cs = HelloText.GetConnectionString();
            var message = HelloText.GetHelloMessage(cs);
            Console.Out.WriteLine(message);
            Console.ReadKey();
        }

    }
}
