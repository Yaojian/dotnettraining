﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HelloWorld
{
    public class HelloMessageManager
    {
        public HelloMessageManager()
            : this(new HelloContext("DotNetTraining"))
        {
        }

        public HelloMessageManager(HelloContext dbContext)
        {
            if (dbContext == null) throw new ArgumentNullException("dbContext");
            m_DbContext = dbContext;
        }

        private readonly HelloContext m_DbContext;

        public List<HelloMessage> GetMessages()
        {
            List<HelloMessage> list = m_DbContext.HelloMessages.ToList();
            return list;
        }

        public void AddMessage(String message)
        {
            var helloMessage = new HelloMessage();
            helloMessage.Id = m_DbContext.HelloMessages.Max(m => m.Id) + 1;
            helloMessage.Message = message;
            m_DbContext.HelloMessages.Add(helloMessage);
            m_DbContext.SaveChanges();
        }

        public void DeleteMessage(int id)
        {
            var message = m_DbContext.HelloMessages.Find(id);
            m_DbContext.HelloMessages.Remove(message);
            m_DbContext.SaveChanges();
        }
    }
}