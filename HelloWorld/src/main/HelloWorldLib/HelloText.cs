﻿using System;
using System.Configuration;
using System.Data.SqlClient;

namespace HelloWorld
{
    public class HelloText
    {
        public static readonly String Message = "Hello, World! (from Lib)";


        public static String GetHelloMessage(String connectionString)
        {
            using (SqlConnection dbc = new SqlConnection(connectionString))
            {
                dbc.Open();

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = dbc;
                    cmd.CommandText = "SELECT Message FROM dbo.HelloMessages";
                    String value = (String)cmd.ExecuteScalar();
                    return value;
                }
            }
        }

        public static string GetConnectionString()
        {
            Configuration c = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            ConnectionStringSettings cs = c.ConnectionStrings.ConnectionStrings["DotNetTraining"];
            string connectionString = cs.ConnectionString;
            return connectionString;
        }
    }
}
