﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace HelloWorld
{
    public class HelloContext : DbContext
    {
        public HelloContext(String nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<StoreGeneratedIdentityKeyConvention>();
        }

        public DbSet<HelloMessage> HelloMessages
        {
            get { return this.Set<HelloMessage>(); }
        } 
    }
}