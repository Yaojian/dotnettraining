﻿using System;

namespace HelloWorld
{
    public class HelloMessage
    {
        public int Id { get; set; }

        public String Message { get; set; }
    }
}