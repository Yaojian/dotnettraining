﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace HelloWorld
{
    [TestFixture]
    public class HelloContextTests
    {
        [Test]
        public void HelloMessages_loads_messages()
        {
            using (var dbc = new HelloContext("DotNetTraining"))
            {
                var count = dbc.HelloMessages.Count();
                Assert.AreEqual(1, count);

                HelloMessage message = dbc.HelloMessages.First();
                Console.WriteLine(message);
                Assert.AreEqual("Hello (from DB)", message.Message);
            }
        }

        [Test]
        public void HelloContext_can_add_message()
        {
            using (var dbc = new HelloContext("DotNetTraining"))
            {
                var newId = dbc.HelloMessages.Max(m => m.Id) + 1;
                var newText = "Message " + newId;
                Console.Out.WriteLine("The new text is: " + newText);

                HelloMessage message = new HelloMessage {Id = newId, Message = newText};
                var oldCount = dbc.HelloMessages.Count();

                dbc.HelloMessages.Add(message); //session.AddOrUpdate
                dbc.SaveChanges();
                Assert.AreEqual(oldCount + 1, dbc.HelloMessages.Count());

                message.Message = "Changed";
                dbc.SaveChanges();

                dbc.HelloMessages.Remove(message);
                dbc.SaveChanges();
                Assert.AreEqual(oldCount, dbc.HelloMessages.Count());
            }
        }
    }
}
