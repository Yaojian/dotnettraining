﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace HelloWorld
{
    [TestFixture]
    public class DbTests
    {
        [Test]
        public void DbConnectionOk()
        {
            Configuration c = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            ConnectionStringSettings cs = c.ConnectionStrings.ConnectionStrings["DotNetTraining"];
            Assert.NotNull(cs);
            Console.Out.WriteLine(cs.ToString());

            using (SqlConnection dbc = new SqlConnection(cs.ConnectionString))
            {
                dbc.Open();

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = dbc;
                    cmd.CommandText = "SELECT 1";
                    int value = (int)cmd.ExecuteScalar();
                    Assert.AreEqual(1, value);
                }
            }
        }

        [Test]
        public void HelloMessages_is_accessable()
        {
            var connectionString = HelloText.GetConnectionString();

            var value = HelloText.GetHelloMessage(connectionString);
            Assert.AreEqual("Hello (from DB)", value);
        }
    }
}
