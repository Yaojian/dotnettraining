﻿using NUnit.Framework;

namespace HelloWorld
{
    [TestFixture]
    public class HelloTextTests
    {
        [Test]
        public void TestMessage()
        {
            Assert.AreEqual("Hello, World! (from Lib)", HelloText.Message);
        }
    }
}